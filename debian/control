Source: fonts-sil-sophia-nubian
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Hideki Yamane <henrich@debian.org>,
Build-Depends: debhelper-compat (= 13),
Standards-Version: 4.5.0
Homepage: https://software.sil.org/SophiaNubian/
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-sophia-nubian.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-sophia-nubian
Rules-Requires-Root: no

Package: fonts-sil-sophia-nubian
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: smart Unicode font family for Nubian languages using Coptic
 Sophia Nubian is a sans serif, Unicode-compliant font based on the SIL Sophia
 (similar to Univers) typeface. Its primary purpose is to provide adequate
 representation for Nubian languages which use the Coptic Unicode character
 set. Since Nubian languages do not use casing, uppercase characters are not
 included in this font. A basic set of Latin glyphs is also provided.
 .
 OpenType and Graphite smart code are available for Nubian macrons and "u"
 vowel.
 .
 Extended font sources are available.
